package models

// User model
type User struct {
	Id string `json:"id"`
	Name string `json:"name"`
	Email string `json:"email"`
	Password string `json:"password"`
	PhoneNumber string `json:"phoneNumber"`
	Date_Of_Birth string `json:"date_of_birth"`
	Gender string `json:"gender"`
	Age  int `json:"age"`
	Father_Name string `json:"father_name"`
	Mother_Name string `json:"mother_name"`
	Mark_Of_Identification string `json:"mark_of_identification"`
	Category string `json:"category"`
	Religion string `json:"religion"`
	Alternate_Contact_No string `json:"alternate_contact_no"`
	Marital_Status string `json:"marital_status"`
	Differently_Abled string `json:"differently_abled"`
	Disability_Category string `json:"disability_category"`
	Employed_Or_Self_Employed string `json:"employed_or_self_employed"`
	Bpl_Number string `json:"bpl_number"`
	Bpl_Or_Apl string `json:"bpl_or_apl"`
	Personal_Income_Annual string `json:"personal_income_annual"`
	Family_Income_Annual string `json:"family_income_annual"`
	Spouse_Income_Annual string `json:"spouse_income_annual"`
	Passport_No string `json:"passport_no"`
	Ration_Card_No string `json:"ration_card_no"`
	Aadhar_No string `json:"aadhar_no"`
	Driving_License_No string `json:"driving_license_no"`
	Pan_Card_No string `json:"pan_card_no"`
}

// user create response model
type UserCreateResponse struct {
	Created bool `json:"created"`
	Id int        `json:"id"`
}
