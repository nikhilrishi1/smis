FROM golang:latest
RUN mkdir -p /go/src/UserMicroservice
ADD . /go/src/UserMicroservice/
WORKDIR /go/src/UserMicroservice/
RUN go get ./
RUN go build -o main .
CMD ["/go/src/UserMicroservice/main"]