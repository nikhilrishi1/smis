package controllers

import (
	"UserMicroservice/models"
	"UserMicroservice/services"
	"UserMicroservice/util"
	"UserMicroservice/constants"
)

type UserController struct {

}

func NewUserController() *UserController {
	return &UserController{}
}


// get user
func (uc *UserController) GetUser(user *models.User) {
	us := services.NewUserService()
	if user.Email != "" {
		us.SUserGet(user)
	}
}
// create user controller
func (uc *UserController) CreateUser(user *models.User) (created bool,insertId int) {
	us := services.NewUserService()
	if user.Email != "" && user.Name != "" {
		exists := us.SUserExists(user)
		if exists {
			return false,-1
		}
		var err error
		insertId, err = us.SCreateUser(user);
		if err != nil {
			util.Fatal(err)
		} else {
			created = true
		}
	}
	services.QPublish(constants.QUserCreated,insertId)
	return created,insertId
}