/*
main file for routing to initiate rpc and http connection

*/

package routes

import (
	"net/http"
	"UserMicroservice/models"
	"encoding/json"
	"UserMicroservice/util"
	"net/rpc"
)


//register http routing
func RegisterHttp() {
	http.HandleFunc("/user",HandleUser)
	http.HandleFunc("/alluser",GetStudents)
	http.HandleFunc("/deleteuser",deleteStudnetHandler)
	http.HandleFunc("/showuser",showStudentHandler)
	http.HandleFunc("/updateStudent",updateStudentHandler)
	
	
}

//register rpc function for two way communications
func RegisterRpc() {
	u := new(Usr)
	err := rpc.Register(u);
	util.Fatal(err)
}

// its a utility function for making standard output
func SendJsonResponseSuccess(w http.ResponseWriter,data interface{}){
	var response models.ResponseModel
	response.Status = http.StatusOK;
	response.Data = data
	responseData,err := json.Marshal(response)
	util.Fatal(err);
	w.Header().Set("Content-Type" ,"application/json");
	w.WriteHeader(http.StatusOK);
	w.Write(responseData);
}

// utility for sending 404 in response
func Send404(w http.ResponseWriter){
	w.WriteHeader(http.StatusNotFound);
	w.Write([]byte(http.StatusText(http.StatusNotFound)));
}