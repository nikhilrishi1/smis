package routes

import (
	"UserMicroservice/models"
	"UserMicroservice/controllers"
	"net/http"
	"encoding/json"
	"UserMicroservice/util"
	"github.com/jinzhu/gorm"
    _ "github.com/jinzhu/gorm/dialects/mysql"
    "fmt"
    "log"
)
var db *gorm.DB
var err error 
//user crud operation based on rest methods
func setConnection() {
	db, err = gorm.Open("mysql", "userms:randompass@tcp(userms.ctm6uenobr4l.us-west-2.rds.amazonaws.com:3306)/userms?charset=utf8")
	   //defer db.Close()
	  if err != nil {
	  fmt.Println(err)
	}
	//fmt.Println("calling get students");
	log.Printf("established the database connection")
}

func HandleUser(w http.ResponseWriter,r *http.Request) {
	setConnection();
	uc := controllers.NewUserController();
	var user models.User
	if(r.Method == http.MethodGet){ // GET call for reading user
		user.Email = r.URL.Query().Get("email");
		uc.GetUser(&user);
		SendJsonResponseSuccess(w,user)
	} else if(r.Method == http.MethodPost){ // POST call for create user
		decoder := json.NewDecoder(r.Body);
		err := decoder.Decode(&user);
		util.Fatal(err)
		db.Create(&user)
		//created,id  := uc.CreateUser(&user)
		//userResponse := models.UserCreateResponse{created,id}
		//SendJsonResponseSuccess(w,userResponse);
		SendJsonResponseSuccess(w,user);
	} else {
		Send404(w) // other methods
	}


}


func GetStudents(w http.ResponseWriter,r *http.Request) {
	setConnection();
	log.Printf("calling get all students list")
	var users []models.User
	db.Find(&users)
	res, err := json.Marshal(users)
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}
	w.Write(res)
		
}

func deleteStudnetHandler(w http.ResponseWriter, r *http.Request) {
	setConnection();
	log.Printf("calling delete Studnet Handler function")
	var deletedUser models.User
	db.Where("id = ?", r.URL.Query().Get("id")).Delete(&deletedUser) // write now this returns a blank item not the deleted item
	res, err := json.Marshal(deletedUser)
	if err != nil {
		http.Error(w, err.Error(), 500)
	}
	w.Write(res)
}

func showStudentHandler(w http.ResponseWriter, r *http.Request) {
	setConnection();
	log.Printf("calling show Studnet Handler function")
	var user models.User
	db.Where("id = ?",r.URL.Query().Get("id")).First(&user)
	res, err := json.Marshal(user)
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}

	w.Write(res)
}

func updateStudentHandler(w http.ResponseWriter, r *http.Request) {
	setConnection();
	log.Printf("calling update Studnet Handler function")
	type body struct {
		Name string 
		Email string 
		Password string 
		PhoneNumber string 
		Date_Of_Birth string 
		Gender string 
		Age  int 
		Father_Name string 
		Mother_Name string 
		Mark_Of_Identification string 
		Category string 
		Religion string 
		Alternate_Contact_No string 
		Marital_Status string 
		Differently_Abled string 
		Disability_Category string 
		Employed_Or_Self_Employed string 
		Bpl_Number string 
		Bpl_Or_Apl string 
		Personal_Income_Annual string 
		Family_Income_Annual string 
		Spouse_Income_Annual string 
		Passport_No string 
		Ration_Card_No string 
		Aadhar_No string 
		Driving_License_No string 
		Pan_Card_No string 
	}
	var updates body
	decoder := json.NewDecoder(r.Body)
	if err := decoder.Decode(&updates); err != nil {
		http.Error(w, err.Error(), 400)
	}
	var updatedUser models.User
	db.Where("id = ?", r.URL.Query().Get("id")).First(&updatedUser)
	updatedUser.Name = updates.Name
	updatedUser.Email = updates.Email
	updatedUser.Password = updates.Password
	updatedUser.PhoneNumber = updates.PhoneNumber
	updatedUser.Date_Of_Birth = updates.Date_Of_Birth
	updatedUser.Gender = updates.Gender
	updatedUser.Age = updates.Age
	updatedUser.Father_Name = updates.Father_Name
	updatedUser.Mother_Name = updates.Mother_Name
	updatedUser.Mark_Of_Identification = updates.Mark_Of_Identification
	updatedUser.Category = updates.Category
	updatedUser.Religion = updates.Religion
	updatedUser.Alternate_Contact_No = updates.Alternate_Contact_No
	updatedUser.Marital_Status = updates.Marital_Status
	updatedUser.Differently_Abled = updates.Differently_Abled 
	updatedUser.Disability_Category = updates.Disability_Category 
	updatedUser.Employed_Or_Self_Employed = updates.Employed_Or_Self_Employed 
	updatedUser.Bpl_Number = updates.Bpl_Number 
	updatedUser.Bpl_Or_Apl = updates.Bpl_Or_Apl 
	updatedUser.Personal_Income_Annual = updates.Personal_Income_Annual 
	updatedUser.Family_Income_Annual = updates.Family_Income_Annual 
	updatedUser.Spouse_Income_Annual = updates.Spouse_Income_Annual 
	updatedUser.Passport_No = updates.Passport_No 
	updatedUser.Ration_Card_No = updates.Ration_Card_No 
	updatedUser.Aadhar_No = updates.Aadhar_No 
	updatedUser.Driving_License_No = updates.Driving_License_No 
	updatedUser.Pan_Card_No = updates.Pan_Card_No
	db.Save(&updatedUser)
	res, err := json.Marshal(updatedUser)
	if err != nil {
		http.Error(w, err.Error(), 500)
	}

	w.Write(res)
}









