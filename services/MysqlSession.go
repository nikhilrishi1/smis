package services

import (
	_ "github.com/go-sql-driver/mysql"
	"database/sql"
	"UserMicroservice/util"
)


var DB *sql.DB

// db session
func GetDbSession() *sql.DB{
	db, err := sql.Open("mysql", "userms:randompass@tcp(userms.ctm6uenobr4l.us-west-2.rds.amazonaws.com:3306)/userms?charset=utf8")
	util.Fatal(err)
	DB = db;
	return db
}

func CloseDB() {
	DB.Close()
}

//func BootStrapDB(){
//	DB.Query("create table users(`id` INT(10) NOT NULL AUTO_INCREMENT,`name` VARCHAR(64) NULL DEFAULT NULL,`email` VARCHAR(64) NULL DEFAULT NULL,`password` VARCHAR(64) NULL DEFAULT NULL,`phoneNumber` VARCHAR(64) NULL DEFAULT NULL,PRIMARY KEY (`id`))")
//
//}