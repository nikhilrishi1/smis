/*
user services
*/

package services

import (
	"database/sql"
	"UserMicroservice/models"
	"fmt"
	"UserMicroservice/services/SqlQuery"
	"UserMicroservice/util"
)

type UserService struct {

}

func NewUserService() *UserService{
	return &UserService{}
}

// get user from database
func(us *UserService) SUserGet(user *models.User){
	querystring := SqlQuery.UserGetQuery

	query := fmt.Sprintf(querystring, user.Email)
	fmt.Println(query)
	rs, err := DB.Query(query)
	util.Fatal(err)
	for rs.Next() {
		ScanUser(user,rs)
	}
	rs.Close()
	return
}

// check user exists in database
func(us *UserService) SUserExists(user *models.User) (exists bool) {

	query := fmt.Sprintf(SqlQuery.UserExistsQuery, user.Name, user.Email)
	rs, err := DB.Query(query)

	util.Fatal(err)
	var count int

	for rs.Next() {
		rs.Scan(&count)
	}

	if count == 1 {
		exists = true
	} else {
		exists = false
	}
	return
}

// user create service
func(us *UserService) SCreateUser(user *models.User) (int, error){
	querystring := SqlQuery.CreateUserQuery;
	query := fmt.Sprintf(querystring,user.Name,user.Email,user.Password,user.PhoneNumber)
	res,err := DB.Exec(query);
	util.Fatal(err);
	insertId,err :=  res.LastInsertId()
	util.Fatal(err)
	return int(insertId),nil

}


// sql row format to struct filling
func ScanUser(u *models.User,rs *sql.Rows)  {
	 rs.Scan(&u.Id,&u.Name, &u.Password, &u.PhoneNumber, &u.Email)
}
